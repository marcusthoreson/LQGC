# Linear-Quadratic-Gaussian Controller

Python implementation of a linear-quadratic-Gaussian controller, consisting of a Kalman filter and linear-quadratic regulator. 

Package is currently in early development, but a functioning proof-of-concept Jupyter notebook can be found here:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/marcusthoreson%2FLQGC/HEAD?filepath=prototype%2Flqgc_1D-Particle.ipynb)
