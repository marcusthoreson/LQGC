# coding=UTF-8
from SimPlant import *

class CtrlUpdateStrategy(object):

  """
   A class for containing alternative methods of updating the SimPlant according to
   control input.

  :version:
  :author:
  """

  def update(self, pl, u):
    """
     Update the SimPlant's state according to control input, u.

    @param SimPlant pl : 
    @param array u : 
    @return  :
    @author
    """
    pass



