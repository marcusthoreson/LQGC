# coding=UTF-8
from Plant import *

class LinearQuadraticRegulator(object):

  """
   

  :version:
  :author:
  """

  def get_control_gains(self, pl, q, r):
    """
     Get the optimal array of control gains, K, given cost function J(Q,R). Requires
     tracking-error and control-effort weight arrays for cost function (Q,R), as well
     as Plant. The lengths of Q,R, and K must be equivalent.

    @param Plant pl : 
    @param array q : 
    @param array r : 
    @return array :
    @author
    """
    pass



