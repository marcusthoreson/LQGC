# coding=UTF-8
from Plant import Plant
from GaussianDist import *
from CtrlUpdateStrategy import *
from SenseUpdateStrategy import *

class SimPlant (Plant):

  """
   A class representing a simulation of some Plant, including Gaussian process and
   sensor noise.

  :version:
  :author:
  """

  """ ATTRIBUTES

   Process noise distribution.

  _proc_noise  (private)

   Sensor noise.

  _sensor_noise  (private)

   

  _init_uncertainty  (private)

   

  _state  (private)

  """

  def __init__(self, proc_noise, sensor_noise, init_uncert, a, b, c):
    """
     SimPlant class constructor. Requires Plant model and process- and sensor-noise
     characteristics (mean, variance).

    @param GaussianDist proc_noise : 
    @param GaussianDist sensor_noise : 
    @param GaussianDist init_uncert : 
    @param array a : 
    @param array b : 
    @param array c : 
    @return  :
    @author
    """
    pass

  def ctrl_update(self, u):
    """
     Simulated control update of the described Plant. This function also updates
     _state appropriately.

    @param array u : 
    @return  :
    @author
    """
    pass

  def measure(self):
    """
     Simulate a measurement from the modelled Plant.

    @return array :
    @author
    """
    pass

  def override_ctrl_update(self, strategy):
    """
     Override the default control update method with a new strategy.

    @param CtrlUpdateStrategy strategy : 
    @return  :
    @author
    """
    pass

  def override_sense_update(self, strategy):
    """
     Override the default method of sensor updating.

    @param SenseUpdateStrategy strategy : 
    @return  :
    @author
    """
    pass

  def reset(self, new_init_uncert = None):
    """
     Reset the state of the SimPlant according to the initial uncertainty.

    @param SimPlant self : 
    @param GaussianDist new_init_uncert : 
    @return  :
    @author
    """
    pass

  def state(self):
    state = self._state
    return state



