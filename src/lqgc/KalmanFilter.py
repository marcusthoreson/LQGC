# coding=UTF-8
from GaussianDist import *
from Plant import *

class KalmanFilter(object):

  """
   

  :version:
  :author:
  """

  def filter(self, pl, prior, u):
    """
     Given a Plant, an a priori belief in the state of the Plant, and some control
     input for this time-step, return an a posteriori belief in the state of the
     Plant.

    @param Plant pl : 
    @param GaussianDist prior : 
    @param double u : 
    @return GaussianDist :
    @author
    """
    pass



