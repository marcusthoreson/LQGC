# coding=UTF-8
from SimPlant import *

class SenseUpdateStrategy(object):

  """
   A class for containing alternative methods of simulating a measurement from the
   SimPlant.

  :version:
  :author:
  """

  def update(self, pl):
    """
     Simulate a measurement from the SimPlant's underlying Plant.

    @param SimPlant pl : 
    @return array :
    @author
    """
    pass



