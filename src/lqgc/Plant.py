# coding=UTF-8

class Plant(object):

  """
   A class that serves as an interface to a dynamic linear system, its response to
   control inputs, and its sensing capabilities.

  :version:
  :author:
  """

  """ ATTRIBUTES

   System dynamics matrix.

  _a  (private)

   System control dynamics.

  _b  (private)

   System sensor dynamics.

  _c  (private)

  """

  def __init__(self, a, b, c):
    """
     Plant class constructor. Requires system dynamics (A, B, C).

    @param array a : 
    @param array b : 
    @param array c : 
    @return  :
    @author
    """
    pass

  def ctrl_update(self, u):
    """
     Update the Plant according to control input, u.

    @param array u : 
    @return  :
    @author
    """
    pass

  def measure(self):
    """
     Take a measurement of the current state of the Plant.

    @return array :
    @author
    """
    pass



