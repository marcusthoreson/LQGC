# coding=UTF-8
from SimPlant import SimPlant
from Controller import Controller
from GaussianDist import GaussianDist
import numpy as np


class Simulation(object):

    """
   A class for simulating the performance of a given Controller in attempting to
   drive a specific plant through a specific trajectory.

  :version:
  :author:
  """

  """ ATTRIBUTES

   The SimPlant used for running this Simulation.

  _pl  (private)

   Controller used for running this Simulation.

  _ctrl  (private)

  """

  def __init__(self, pl: SimPlant, ctrl: Controller):
      """
     Constructor for Simulation class. Requires one instance each of SimPlant and
     Controller.

    @param SimPlant pl : 
    @param Controller ctrl : 
    @return  :
    @author
    """
    self._pl = pl
    pl.reset()
    self._ctrl = ctrl
    self._record = [tuple()]

  def reset(self, init_state_uncert: GaussianDist = None):
    self._pl.reset(init_state_uncert)
    self._record.clear()

  def record_sim_state(self, state: tuple):
    self._record.append(state)

  def run(self, target_trajectory: list):
    """
    Given a SimPlant and a trajectory of target points in time-space, return 1) an
    array of time-space coordiantes representing the actual trajectory of the Plant
    and 2) an array of time-space coordinates representing the estimated trajectory
    of the of the Plant.

    @param list target_trajectory : 
    @return tuple : 
    @author
    """
    p_0 = target_trajectory[0]
    t_0 = p_0[0]
    x_0 = self._pl.state()
    sim_state = (t_0, x_0, t_0, p_0[0])
    self.record_sim_state(sim_state)

    
    return self._record



