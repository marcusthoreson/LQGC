# coding=UTF-8
from Plant import *
from GaussianDist import *
from KalmanFilter import *
from LinearQuadraticRegulator import *

class Controller(object):

  """
   A class used to aggregate a KalmanFilter and LinearQuadraticRegulator into a
   common controller bound to a given plant.

  :version:
  :author:
  """

  """ ATTRIBUTES

   Kalman filter linear state-estimator

  _kf  (private)

   

  _lqr  (private)

  """

  def __init__(self):
    """
     Constructor for Controller class.

    @return  :
    @author
    """
    pass

  def sched_ctrl_steps(self, pl, target):
    """
     Given a Plant and a target state for the Plant, select an appropriate control
     frequency, select weighting matrices for tracking error and control effort (Q
     and R), and use the LQR to calculate control gains.

    @param Plant pl : 
    @param tuple target : 
    @return array :
    @author
    """
    pass

  def ctrl_plant(self, pl, prior_state_est):
    """
     Drive the given plant to some desired state

    @param Plant pl : 
    @param GaussianDist prior_state_est : Frequency at which cobntrol can be exerted on the given plant, pl
    @return GaussianDist :
    @author
    """
    pass



