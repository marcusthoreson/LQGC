import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

version_file = open('VERSION')
version = version_file.read().strip()

setuptools.setup(
    name="lqgc-marcusthoreson",
    version=version,
    author="Marcus Thoreson",
    description="Linear-quadratic-Gaussian controller implementation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/marcusthoreson/LQGC",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src")
)
